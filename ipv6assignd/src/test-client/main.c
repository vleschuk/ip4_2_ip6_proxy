#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <uv.h>

#define SERVER_TCP_HOST   "127.0.0.1"
#define SERVER_TCP_PORT   12345
#define SERVER_UNIX_SOCK  "/tmp/ipv6.sock"

#define ERREXIT() { fprintf( stderr, "Usage:\n" );\
                    fprintf( stderr, "\t%s [-r] [-a ipv6addr] ( release IP address )\n", argv[0] );\
                    fprintf( stderr, "\t%s [-l] ( obtain IP address )\n\n", argv[0] );\
                    fprintf( stderr, "\t-u - use UNIX domain socket, optional flag\n" );\
                    fprintf( stderr, "\tserver socket values: tcp->%s:%d, unix->%s\n",\
                    SERVER_TCP_HOST, SERVER_TCP_PORT, SERVER_UNIX_SOCK );\
                    exit(1); }

enum {
    LEASE     = 1,
    RELEASE   = 2,
    LEASE_OK  = 4,
    LEASE_ERR = 8,
    UNIX_SOCK = 16
};

typedef struct {
    int cmd;
    struct in6_addr ip;
} msg_proto_t;

typedef struct {
    uv_write_t req;
    uv_buf_t buf;
} write_req_t;

struct in6_addr release_ip;

void alloc_buffer( uv_handle_t *, size_t, uv_buf_t * );
void on_close( uv_handle_t * );
void on_read( uv_stream_t *, ssize_t, const uv_buf_t * );
void on_write_lease( uv_write_t *, int );
void on_write_release( uv_write_t *, int );
void send_lease( uv_connect_t *, int );
void send_release( uv_connect_t *, int );

int main( int argc, char **argv )
{
    int opt, flag, ip_set;
    uv_tcp_t *tcp_socket;
    uv_pipe_t *unix_socket;
    uv_connect_t *connect;
    struct sockaddr_in srv;

    if( argc == 1 )
    {
        ERREXIT();
    }

    flag = 0;
    ip_set = 0;
    while(( opt = getopt( argc, argv, "ulra:" )) != -1 )
    {
        switch( opt )
        {
            case 'l':
                flag = ( flag & UNIX_SOCK ) ? LEASE | UNIX_SOCK : LEASE;
                break;
            case 'r':
                flag = ( flag & UNIX_SOCK ) ? RELEASE | UNIX_SOCK : RELEASE;
                break;
            case 'u':
                flag |= UNIX_SOCK;
                break;
            case 'a':
                if( inet_pton( AF_INET6, optarg, &release_ip ) == 0 )
                {
                    fprintf( stderr, "Invalid IPv6 address\n");
                    exit(1);
                }
                ip_set = 1;
                break;
            default:
                ERREXIT();
        }
    }

    if( flag & UNIX_SOCK )
    {
        unix_socket = ( uv_pipe_t * ) malloc( sizeof( uv_pipe_t ));
        uv_pipe_init( uv_default_loop(), unix_socket, 0 );
    }
    else
    {
        tcp_socket = ( uv_tcp_t * ) malloc( sizeof( uv_tcp_t ));
        uv_tcp_init( uv_default_loop(), tcp_socket );
    }

    connect = ( uv_connect_t * ) malloc( sizeof( uv_connect_t ));

    if( flag & LEASE )
    {
        if( flag & UNIX_SOCK )
        {
            uv_pipe_open( unix_socket, socket( AF_UNIX, SOCK_STREAM, 0 ));
            uv_pipe_connect( connect, unix_socket, SERVER_UNIX_SOCK, send_lease );
        }
        else
        {
            uv_ip4_addr( SERVER_TCP_HOST, SERVER_TCP_PORT, &srv );
            uv_tcp_connect( connect, tcp_socket, ( const struct sockaddr * ) &srv, send_lease );
        }
    }
    else if( flag & RELEASE )
    {
        if( !ip_set )
        {
            fprintf( stderr, "IPv6 address was not specified\n" );
            exit(1);
        }
        if( flag & UNIX_SOCK )
        {
            uv_pipe_open( unix_socket, socket( AF_UNIX, SOCK_STREAM, 0 ));
            uv_pipe_connect( connect, unix_socket, SERVER_UNIX_SOCK, send_release );
        }
        else
        {
            uv_ip4_addr( SERVER_TCP_HOST, SERVER_TCP_PORT, &srv );
            uv_tcp_connect( connect, tcp_socket, ( const struct sockaddr * ) &srv, send_release );
        }
    }
    else
    {
        ERREXIT();
    }

    return uv_run(uv_default_loop(), UV_RUN_DEFAULT );
}

void alloc_buffer( uv_handle_t *handle, size_t size, uv_buf_t *buf )
{
    ( void ) handle;
    *buf = uv_buf_init(( char * ) malloc( size ), size );
}

void on_close( uv_handle_t *handle )
{
    free( handle );
}

void on_read( uv_stream_t *client, ssize_t nread, const uv_buf_t *buf )
{
    msg_proto_t *msg;
    char ipstr[INET6_ADDRSTRLEN];

    if( nread < 0 )
    {
        if( nread != UV_EOF )
        {
            fprintf( stderr, "Read error %s\n", uv_strerror( nread ));
        }
        uv_close(( uv_handle_t *) client, on_close );
        if( buf->base )
        {
            free( buf->base );
        }
        return;
    }

    if( nread != sizeof( msg_proto_t ))
    {
        fprintf( stderr, "Ivalid data size\n" );
        uv_close(( uv_handle_t *) client, on_close );
        free( buf->base );
        return;
    }

    msg = ( msg_proto_t * ) buf->base;
    if( msg->cmd & LEASE_OK )
    {
         inet_ntop( AF_INET6, &msg->ip, ipstr, INET6_ADDRSTRLEN );
         printf( "Recieve address %s\n", ipstr );
    }
    if( msg->cmd & LEASE_ERR )
    {
         fprintf( stderr, "No free ip addresses now\n" );
    }

    free( buf->base );
}

void on_write_lease( uv_write_t *req, int status )
{
    write_req_t *wr;

    wr = ( write_req_t *) req;
    free( wr->buf.base );
    if( status < 0 )
    {
        fprintf( stderr, "Connection failed %s\n", uv_strerror( status));
        return;
    }
    uv_read_start( req->handle, alloc_buffer, on_read );
    free( req );
}

void on_write_release( uv_write_t *req, int status )
{
    write_req_t *wr;

    wr = ( write_req_t *) req;
    free( wr->buf.base );
    if( status < 0 )
    {
        fprintf( stderr, "Connection failed %s\n", uv_strerror( status));
        return;
    }
    free( req );
}

void send_lease( uv_connect_t *dest, int status )
{
    write_req_t *req;
    msg_proto_t msg;

    if( status < 0 )
    {
        fprintf( stderr, "Connection failed %s\n", uv_strerror( status ));
        free( dest->handle );
        free( dest );
        return;
    }

    msg.cmd = LEASE;

    req = ( write_req_t * ) malloc( sizeof( write_req_t ));
    req->buf = uv_buf_init(( char * ) malloc( sizeof( msg_proto_t )), sizeof( msg_proto_t ));
    memcpy( req->buf.base, &msg, sizeof( msg_proto_t ));
    uv_write(( uv_write_t * ) req, ( uv_stream_t * ) dest->handle, &req->buf, 1, on_write_lease );

    free( dest );
}

void send_release( uv_connect_t * dest, int status )
{
    write_req_t *req;
    msg_proto_t msg;

    if( status < 0 )
    {
        fprintf( stderr, "Connection failed %s\n", uv_strerror( status ));
        free( dest->handle );
        free( dest );
        return;
    }

    msg.cmd = RELEASE;
    msg.ip = release_ip;

    req = ( write_req_t * ) malloc( sizeof( write_req_t ));
    req->buf = uv_buf_init(( char * ) malloc( sizeof( msg_proto_t )), sizeof( msg_proto_t ));
    memcpy( req->buf.base, &msg, sizeof( msg_proto_t ));
    uv_write(( uv_write_t * ) req, ( uv_stream_t * ) dest->handle, &req->buf, 1, on_write_release );

    free( dest );
}
