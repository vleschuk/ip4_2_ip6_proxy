/* vim: set tabstop=4 shiftwidth=4 expandtab : */
#ifndef LISTEN_H_
#define LISTEN_H_

#include <read_config.h>

/** @brief start listen for tcp connections */
int wait_for_clients( daemon_data_t * );

#endif /* LISTEN_H_ */
