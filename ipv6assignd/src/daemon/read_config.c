/* vim: set tabstop=4 shiftwidth=4 expandtab : */
#include <string.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <errno.h>
#include <libconfig.h>

#include <read_config.h>
#include <ipv6_calc.h>

int read_config( daemon_data_t *data )
{
    int res, network_set;
    config_t cfg;
    const char *value;
    char *token, *saveptr;
    int mask;
    struct in6_addr source_addr;
    char source_addr_str[INET6_ADDRSTRLEN];
    char network[BUFSIZ];
    res = network_set = 0;

    memset( data->cfg, 0, sizeof( daemon_config_t ));
    memset( &source_addr, 0, sizeof( source_addr ));
    memset( source_addr_str, 0, sizeof( source_addr_str ));
    memset( network, 0, sizeof( network ));

    config_init( &cfg );

    if( !config_read_file( &cfg, data->cfg_path ))
    {
        syslog( LOG_ERR, "%s %d - %s", config_error_file( &cfg ),
                config_error_line( &cfg ), config_error_text( &cfg ));
        res =  1;
        goto cleanup;
    }

    if( config_lookup_string( &cfg, "ip6_network", &value ) != CONFIG_FALSE )
    {
        strncpy( network, value, sizeof( network ));
        token = strtok_r( network, "/", &saveptr );
        if( token == NULL )
        {
            syslog( LOG_ERR, "incorrect network address, must be ipv6_addr/cidr" );
        }
        else
        {
            strncpy( source_addr_str, token, INET6_ADDRSTRLEN );
            if( inet_pton( AF_INET6, source_addr_str, &source_addr ) == 0 )
            {
                syslog( LOG_ERR, "Invalid IPv6 network address" );
            }
            else
            {
                token = strtok_r( NULL, "/", &saveptr );
                if( token == NULL )
                {
                    syslog( LOG_ERR, "incorrect network address, must be ipv6_addr/cidr" );
                }
                else
                {
                    mask = atoi( token );
                    if( mask > 128 || mask < 0 )
                    {
                        syslog( LOG_ERR, "incorrect network CIDR, must be [0..128]" );
                    }
                    else
                    {
                        get_min_and_max_ip( &data->cfg->ip_range.ip_start, &data->cfg->ip_range.ip_end,
                                            &source_addr, mask );
                        network_set = 1;
                    }
                }
            }
        }
    }

    if( !network_set )
    {
        if( config_lookup_string( &cfg, "ip6_start", &value ) == CONFIG_FALSE )
        {
            syslog( LOG_ERR, "The ip6_start address is not set" );
            res =  1;
            goto cleanup;
        }
        else
        {
            if( inet_pton( AF_INET6, value, &data->cfg->ip_range.ip_start ) == 0 )
            {
                syslog( LOG_ERR, "Invalid IPv6 address start" );
                res =  1;
                goto cleanup;
            }
        }

        if( config_lookup_string( &cfg, "ip6_end", &value ) == CONFIG_FALSE )
        {
            syslog( LOG_ERR, "The ip6_end address is not set" );
            res =  1;
            goto cleanup;
        }
        else
        {
            if( inet_pton( AF_INET6, value, &data->cfg->ip_range.ip_end ) == 0 )
            {
                syslog( LOG_ERR, "Invalid IPv6 address end" );
                res =  1;
                goto cleanup;
            }
        }
    }

    if( config_lookup_string( &cfg, "host", &value ) == CONFIG_FALSE )
    {
        strncpy( data->cfg->host, DEFAULT_DAEMON_HOST, DAEMON_HOST_LEN );
    }
    else
    {
        ( strlen( value ) == 0 ) ? strncpy( data->cfg->host, DEFAULT_DAEMON_HOST, DAEMON_HOST_LEN ) :
            strncpy( data->cfg->host, value, DAEMON_HOST_LEN );
    }

    data->cfg->link_family = strstr( data->cfg->host, ".sock" ) ? AF_UNIX : AF_INET;

    if( config_lookup_int( &cfg, "port", &data->cfg->port ) == CONFIG_FALSE )
    {
        data->cfg->port = DEFAULT_DAEMON_PORT;
    }

    if( config_lookup_int( &cfg, "percent", &data->cfg->percent ) == CONFIG_FALSE )
    {
        data->cfg->port = DEFAULT_PERCENT;
    }

    if( config_lookup_string( &cfg, "mongodb_uri", &value ) == CONFIG_FALSE )
    {
        strncpy( data->cfg->mongo_uri, DEFAULT_MONGO_URI, MONGO_URI_LEN );
    }
    else
    {
        ( strlen( value ) == 0 ) ? strncpy( data->cfg->mongo_uri, DEFAULT_MONGO_URI, MONGO_URI_LEN ) :
            strncpy( data->cfg->mongo_uri, value, MONGO_URI_LEN );
    }

    if( config_lookup_string( &cfg, "mongodb_name", &value ) == CONFIG_FALSE )
    {
        strncpy( data->cfg->mongo_db, DEFAULT_MONGO_DB, MONGO_DB_LEN );
    }
    else
    {
        ( strlen( value ) == 0 ) ? strncpy( data->cfg->mongo_db, DEFAULT_MONGO_DB, MONGO_DB_LEN ) :
            strncpy( data->cfg->mongo_db, value, MONGO_DB_LEN );
    }

cleanup:
    config_destroy( &cfg );
    return res;
}
