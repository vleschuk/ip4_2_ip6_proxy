# README #
This is a socks5 tcp proxy mapping incoming ipv4 ports to different ipv6 outgoing addrs: listens for tcp connection on 1 ipv4 address and multiple ports and than connects to upstream host via ipv6 addrs. The proxy uses [libuv](https://github.com/libuv/libuv) library for event-loop and multi-process paradigm to gain advantage from multi-core systems (by default the number of worker children is equal to number of cpu cores).

Outgoing ipv6 addrs change every N seconds (set in config).

### Licensing ###

This is a **free** software distributed on terms of [WTFPL](http://www.wtfpl.net/about/).

### How do I get set up? ###

* First clone the repository: 

```
#!bash
git clone git@bitbucket.org:vleschuk/ip4_2_ip6_proxy.git
cd ip4_2_ip6_proxy
```
* build using cmake:

```
#!bash
mkdir build
cd build
cmake ..
make
```

* Running: to run proxy you can specify options in command-line:


```
#!bash
Usage: tcp_proxy [options]
options:
	-c file	-	path to config file (other options will be ignored)
	-h host	-	listen host address (default: 127.0.0.1)
	-p port	-	listen host port (default: 80)
	-H host	-	upstream host address (default: 127.0.0.1)
	-P port	-	upstream host port (default: 8080)
	-t seconds	-	client timeout (default: 5)
	-T seconds	-	upstream timeout (default: 5)
	-w integer	-	number of worker processes, 0 - num of cpus (default: 0)

```

or via config file (option -c), sample of config file is located in /sample subdir of the project.

* Dependencies: the project uses libuv and libconfig, however they are already included in project in 3rdparty dir. The only requirement is gcc compiler, autotools (to build libconfig and libuv) and cmake >= 2.8. NOTE: code uses gnu99 C extensions, thus compiler must support -std=gnu99.

# Environment #
To make it work with large number of listening ports and ipv6 addresses the following params need to be changed. (Example is for CentOS 6):

* net.ipv6.conf.all.max_addresses = 30000 # in /etc/sysctl.conf
* session    required     pam_limits.so # in /etc/pam.d/login
* root	soft	nofile	655360 # in /etc/security/limits.d/80-nofile.conf
* root	hard	nofile	655360 # in /etc/security/limits.d/80-nofile.conf