/* vim: set tabstop=4 shiftwidth=4 expandtab : */
#ifndef TCP_PROXY_CONFIG_H_
#define TCP_PROXY_CONFIG_H_

#define DEFAULT_PROXY_HOST "127.0.0.1"
#define DEFAULT_PROXY_PORT 80
#define DEFAULT_PROXY_OUT_IF "eth0"
#define DEFAULT_DHCP_HOST "127.0.0.1"
#define DEFAULT_DHCP_PORT 12345
#define DEFAULT_PROXY_CLIENT_TIMEOUT 5 /* seconds */
#define DEFAULT_PROXY_UPSTREAM_TIMEOUT 5 /* seconds */
#define DEFAULT_PARENT_TIMEOUT 5 /* seconds */
#define DEFAULT_MONGO_URI    "mongodb://localhost:27017"
#define DEFAULT_MONGO_DB     "ipv6assignd"
#define DEFAULT_BIND_ADDR_CHANGE_TIMEOUT 30 /* seconds */
#define DEFAULT_PROXY_NUM_WORKERS 0
#define PROXY_HOST_LEN 256
#define MONGO_DB_LEN         128
#define MONGO_URI_LEN        2048

#ifndef config_error_file
#define config_error_file(C) ""
#endif /* config_error_file */

typedef struct _tcp_proxy_config
{
    char host[PROXY_HOST_LEN];
    char dhcpd_host[PROXY_HOST_LEN];
    char mongo_uri[MONGO_URI_LEN];
    char mongo_db[MONGO_DB_LEN];
    long *port_range;
    long ports_count;
    long dhcpd_port;
    long dhcpd_sock_family;
    long client_timeout; /* seconds, 0 for default */
    long upstream_timeout; /* seconds, 0 for default */
    long parent_timeout; /* seconds, 0 for default */
    long addr_change_timeout; /* seconds, 0 for default */
    long num_workers; /* 0 for default */
    char **out_ifs; /* will be allocated during config parse */
    long num_ifs; /* 1 for default */
    uint8_t daemon_mode; /* 1(enable) for default */
    uint8_t socks5_auth; /* 1(enable) for default */
} tcp_proxy_config_t;

/* 
 * Allocate config in shared memory
 * Return -1 on error and 0 on success
 */
int alloc_config();

/*
 * Open read-only config
 * Return -1 on error and 0 on success
 */
int open_config();

void free_config();

/*
 * Set default config values. Assuming config already allocated
 */
void set_config_defaults();

/*
 * Get config filename
 */
const char *get_config_filename();

tcp_proxy_config_t *get_config();

/* 
 * Config initialization
 * Return -1 on error and 0 on success
 */
int parse_command_line(int argc, char **argv);
int parse_config_file(const char *filename);

/* debug */
void dump_config();
#endif /* TCP_PROXY_CONFIG_H_ */
