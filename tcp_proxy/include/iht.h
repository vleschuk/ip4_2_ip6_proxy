/* vim: set tabstop=4 shiftwidth=4 expandtab : */
#ifndef IHT_H_
#define IHT_H_

typedef unsigned long iht_key_t;

void *iht_create(void);

/* node pointer must be a pointer to a structure starting with int_key_t member */
void *iht_insert(void *iht, void *node);

void *iht_find(void *iht, iht_key_t key);

void iht_destroy(void *iht);

void print_table(void *iht);

#endif /* IHT_H_ */
