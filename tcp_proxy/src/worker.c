/* vim: set tabstop=4 shiftwidth=4 expandtab : */
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <libgen.h>

#include "defs.h"
#include "util.h"
#include "config.h"
#include "s5.h"
#include "dhcpc.h"
#include "manage_mongo_db.h"

/* A connection is modeled as an abstraction on top of two simple state
 * machines, one for reading and one for writing.  Either state machine
 * is, when active, in one of three states: busy, done or stop; the fourth
 * and final state, dead, is an end state and only relevant when shutting
 * down the connection.  A short overview:
 *
 *                          busy                  done           stop
 *  ----------|---------------------------|--------------------|------|
 *  readable  | waiting for incoming data | have incoming data | idle |
 *  writable  | busy writing out data     | completed write    | idle |
 *
 * We could remove the done state from the writable state machine. For our
 * purposes, it's functionally equivalent to the stop state.
 *
 * When the connection with upstream has been established, the client_ctx
 * moves into a state where incoming data from the client is sent upstream
 * and vice versa, incoming data from upstream is sent to the client.  In
 * other words, we're just piping data back and forth.  See conn_cycle()
 * for details.
 *
 * An interesting deviation from libuv's I/O model is that reads are discrete
 * rather than continuous events.  In layman's terms, when a read operation
 * completes, the connection stops reading until further notice.
 *
 * The rationale for this approach is that we have to wait until the data
 * has been sent out again before we can reuse the read buffer.
 *
 * It also pleasingly unifies with the request model that libuv uses for
 * writes and everything else; libuv may switch to a request model for
 * reads in the future.
 */
enum conn_state {
    c_busy,  /* Busy; waiting for incoming data or for a write to complete. */
    c_done,  /* Done; read incoming data or write finished. */
    c_stop,  /* Stopped. */
    c_dead
};

/* Session states. */
enum sess_state {
    s_handshake,        /* Wait for client handshake. */
    s_handshake_auth,   /* Waith for client auth data. */
    s_req_start,        /* Start waiting for request data. */
    s_req_parse,        /* Wait for request data. */
    s_req_lookup,       /* Wait for upstream hostname DNS lookup to complete. */
    s_req_connect,      /* Wait for uv_tcp_connect() to complete. */
    s_proxy_start,      /* Connected. Start piping data. */
    s_proxy,            /* Connected. Pipe data back and forth. */
    s_kill,             /* Tear down session. */
    s_almost_dead_0,    /* Waiting for finalizers to complete. */
    s_almost_dead_1,    /* Waiting for finalizers to complete. */
    s_almost_dead_2,    /* Waiting for finalizers to complete. */
    s_almost_dead_3,    /* Waiting for finalizers to complete. */
    s_almost_dead_4,    /* Waiting for finalizers to complete. */
    s_dead              /* Dead. Safe to free now. */
};

static const char *strconnstate(int state) {
    switch(state) {
    case c_busy: return "c_busy";
    case c_done: return "c_done";
    case c_stop: return "c_stop";
    case c_dead: return "c_dead";
    default: return "c_unknown";
    }
}

static const char *strstate(int state) {
    switch(state) {
    case s_handshake: return "s_handshake";
    case s_handshake_auth: return "s_handshake_auth";
    case s_req_start: return "s_req_start";
    case s_req_parse: return "s_req_parse";
    case s_req_lookup: return "s_req_lookup";
    case s_req_connect: return "s_req_connect";
    case s_proxy_start: return "s_proxy_start";
    case s_proxy: return "s_proxy";
    case s_kill: return "s_kill";
    case s_almost_dead_0: return "s_almost_dead0";
    case s_almost_dead_1: return "s_almost_dead_1";
    case s_almost_dead_2: return "s_almost_dead_2";
    case s_almost_dead_3: return "s_almost_dead_3";
    case s_almost_dead_4: return "s_almost_dead_4";
    case s_dead: return "s_dead";
    default: return "Unknown state";
    }
}

static const char *progname = __FILE__; /* reset in main() */
static const tcp_proxy_config_t *cfg = NULL;

static uv_loop_t *loop;
static uv_pipe_t queue;

static pid_t parent_pid;
static uv_timer_t check_parent_timer_req;

static mongoc_client_pool_t *pool = NULL;
static mongoc_uri_t *uri = NULL;

static void on_new_connection(uv_stream_t *q, ssize_t nread, const uv_buf_t *buf);
static void do_next(client_ctx *cx);
static int do_handshake(client_ctx *cx);
static int do_handshake_auth(client_ctx *cx);
static int do_req_start(client_ctx *cx);
static int do_req_parse(client_ctx *cx);
static int do_req_lookup(client_ctx *cx);
static int do_req_connect_start(client_ctx *cx);
static int do_req_connect(client_ctx *cx);
static int do_proxy_start(client_ctx *cx);
static int do_proxy(client_ctx *cx);
static int do_kill(client_ctx *cx);
static int do_almost_dead(client_ctx *cx);
static int conn_cycle(const char *who, conn *a, conn *b);
static void conn_timer_reset(conn *c);
static void conn_timer_expire(uv_timer_t *handle);
static void conn_getaddrinfo(conn *c, const char *hostname);
static void conn_getaddrinfo_done(uv_getaddrinfo_t *req,
                                  int status,
                                  struct addrinfo *ai);
static int conn_connect(conn *c);
static void conn_connect_done(uv_connect_t *req, int status);
static void pipe_alloc(uv_handle_t *handle, size_t suggested_size, uv_buf_t *buf);
static void conn_read(conn *c);
static void conn_read_done(uv_stream_t *handle,
                           ssize_t nread,
                           const uv_buf_t *buf);
static void conn_alloc(uv_handle_t *handle, size_t size, uv_buf_t *buf);
static void conn_write(conn *c, const void *data, unsigned int len);
static void conn_write_done(uv_write_t *req, int status);
static void conn_close(conn *c);
static void conn_close_done(uv_handle_t *handle);
static void handle_stop(uv_signal_t *handle, int signum);
static void check_parent_callback(uv_timer_t *handle);
static void init_signals();
static void cleanup();

int main(int argc, char **argv) {
    progname = basename(argv[0]);

    parent_pid = getppid();

    if(-1 == open_config()) {
        pr_err("Failed to open configuration");
        exit(EXIT_FAILURE);
    }

    if(0 != atexit(cleanup)) {
        pr_err("Can't register cleanup function");
        exit(EXIT_FAILURE);
    }

    cfg = get_config();
    mongoc_init();
    if(( uri = mongoc_uri_new( cfg->mongo_uri )) == NULL )
    {
        pr_err( "Failed to parse a string containing a MongoDB style URI." );
        exit( EXIT_FAILURE );
    }
    pool = mongoc_client_pool_new( uri );

    loop = uv_default_loop();

    init_signals();
    uv_pipe_init(loop, &queue, 1 /* ipc */);
    int err = uv_pipe_open(&queue, 0);
    if(err) {
        pr_err("Failed to open pipe with parent: %s", uv_err_name(err));
        exit(EXIT_FAILURE);
    }

    CHECK(0 == uv_timer_init(loop, &check_parent_timer_req));
    uv_timer_start(&check_parent_timer_req, check_parent_callback, cfg->parent_timeout*1000, cfg->parent_timeout*1000);

    uv_read_start((uv_stream_t*)&queue, pipe_alloc, on_new_connection);
    return uv_run(loop, UV_RUN_DEFAULT);
}

void on_new_connection(uv_stream_t *q, ssize_t nread, const uv_buf_t *buf) {
    if (nread < 0) {
        if (nread != UV_EOF) {
            pr_err("Read error %s\n", uv_err_name(nread));
        }
        uv_close((uv_handle_t*) q, NULL);
        return;
    }

    /* BUG: strange thing: this debug print reports 65536 bytes read instead of 1 */
    pr_debug("Read from pipe: '%s' (%zu bytes, %d)", buf->base, buf->len, nread);

    uv_pipe_t *pipe = (uv_pipe_t*) q;
    if (!uv_pipe_pending_count(pipe)) {
        pr_err("No pending count\n");
        return;
    }

    uv_handle_type pending = uv_pipe_pending_type(pipe);
    assert(pending == UV_TCP);

    client_ctx *cx = xcalloc(1, sizeof(*cx));
    conn *incoming;
    conn *outgoing;

    pr_debug("Initiating tcp connection");
    CHECK(0 == uv_tcp_init(loop, &cx->incoming.handle.tcp));
    CHECK(0 == uv_accept(q, &cx->incoming.handle.stream));
    uv_os_fd_t fd;
    uv_fileno((const uv_handle_t*)&cx->incoming.handle.handle, &fd);
    pr_info("Accepted fd %d %d", getpid(), fd);
    cx->state = s_handshake;
    s5_init(&cx->parser);

    incoming = &cx->incoming;
    incoming->client = cx;
    incoming->result = 0;
    incoming->rdstate = c_stop;
    incoming->wrstate = c_stop;
    incoming->idle_timeout = cfg->client_timeout * 1000;
    CHECK(0 == uv_timer_init(loop, &incoming->timer_handle));

    outgoing = &cx->outgoing;
    outgoing->client = cx;
    outgoing->result = 0;
    outgoing->rdstate = c_stop;
    outgoing->wrstate = c_stop;
    outgoing->idle_timeout = cfg->upstream_timeout * 1000;
    outgoing->addr_is_released = 0;
    if (nread > 1 && (buf->base[0] != 'n')) {
        outgoing->leased_addr = strdup(buf->base);
        outgoing->leased_addr[nread] = '\0';
    }
    else {
        outgoing->leased_addr = 0;
    }
    CHECK(0 == uv_tcp_init(loop, &outgoing->handle.tcp));
    CHECK(0 == uv_timer_init(loop, &outgoing->timer_handle));

    /* Waiting for initial packet */
    conn_read(incoming);
    char *b = ((uv_buf_t *)buf)->base;
    if(b) {
        free(b);
        b = 0;
    }
}

/* 
 * This is the core state machine that drives the client <-> upstream proxy.
 */
static void do_next(client_ctx *cx) {
    int new_state;
    pr_debug("%s: state: %d(%s)", __func__, cx->state, strstate(cx->state));
    pr_debug("%s: cx_in_rdstate: %d(%s), cx_in_wrstate: %d(%s) cx_out_rdstate: %d(%s), cx_out_wrstate: %d(%s)",
          __func__,
          cx->incoming.rdstate, strconnstate(cx->incoming.rdstate),
          cx->incoming.wrstate, strconnstate(cx->incoming.wrstate),
          cx->outgoing.rdstate, strconnstate(cx->outgoing.rdstate),
          cx->outgoing.wrstate, strconnstate(cx->outgoing.wrstate));


    ASSERT(cx->state != s_dead);
    switch (cx->state) {
      case s_handshake:
        new_state = do_handshake(cx);
        break;
      case s_handshake_auth:
        new_state = do_handshake_auth(cx);
        break;
      case s_req_start:
        new_state = do_req_start(cx);
        break;
    case s_req_parse:
        new_state = do_req_parse(cx);
        break;
      case s_req_lookup:
        new_state = do_req_lookup(cx);
        break;
      case s_req_connect:
        new_state = do_req_connect(cx);
        break;
      case s_proxy_start:
        new_state = do_proxy_start(cx);
        break;
      case s_proxy:
        new_state = do_proxy(cx);
        break;
      case s_kill:
        new_state = do_kill(cx);
        break;
      case s_almost_dead_0:
      case s_almost_dead_1:
      case s_almost_dead_2:
      case s_almost_dead_3:
      case s_almost_dead_4:
        new_state = do_almost_dead(cx);
        break;
      default:
        pr_err("invalid state: %d", cx->state);
        UNREACHABLE();
    }
    cx->state = new_state;

    if (cx->state == s_dead) {
      free(cx);
    }
}

static int do_handshake(client_ctx *cx) {
    unsigned int methods;
    conn *incoming;
    s5_ctx *parser;
    uint8_t *data;
    size_t size;
    int err;

    parser = &cx->parser;
    incoming = &cx->incoming;
    ASSERT(incoming->rdstate == c_done);
    ASSERT(incoming->wrstate == c_stop);
    incoming->rdstate = c_stop;

    if (incoming->result < 0) {
        pr_err("read error: %s", uv_strerror(incoming->result));
        return do_kill(cx);
    }

    data = (uint8_t *) incoming->t.buf;
    size = (size_t) incoming->result;
    err = s5_parse(parser, &data, &size);
    if (err == s5_ok) {
        conn_read(incoming);
        return s_handshake;    /* Need more data. */
    }

    if (size != 0) {
        /* Could allow a round-trip saving shortcut here if the requested auth
         * method is S5_AUTH_NONE (provided unauthenticated traffic is allowed.)
         * Requires client support however.
         */
        pr_err("junk in handshake");
        return do_kill(cx);
    }

    if (err != s5_auth_select) {
        pr_err("handshake error: %s", s5_strerror(err));
        return do_kill(cx);
    }

    methods = s5_auth_methods(parser);
    if (!cfg->socks5_auth && (methods & S5_AUTH_NONE)) {
        pr_debug("select noauth");
        s5_select_auth(parser, S5_AUTH_NONE);
        conn_write(incoming, "\5\0", 2);    /* No auth required. */
        return s_req_start;
    }

    if (cfg->socks5_auth && (methods & S5_AUTH_PASSWD)) {
        pr_debug("select password auth");
        s5_select_auth(parser, S5_AUTH_PASSWD);
        conn_write(incoming, "\5\2", 2);    /* No auth required. */
        return s_handshake_auth;
    }

    pr_debug("No acceptable auth");
    conn_write(incoming, "\5\377", 2);    /* No acceptable auth. */
    return s_kill;
}

/* TODO(bnoordhuis) Implement username/password auth. */
static int do_handshake_auth(client_ctx *cx) {
    pr_debug(__func__);

    conn *incoming;
    s5_ctx *parser;
    uint8_t *data;
    size_t size;
    int err;

    parser = &cx->parser;
    incoming = &cx->incoming;
    incoming->rdstate = c_stop;

    if (incoming->result < 0) {
        pr_err("read error: %s", uv_strerror(incoming->result));
        return do_kill(cx);
    }

    data = (uint8_t *) incoming->t.buf;
    size = (size_t) incoming->result;
    err = s5_parse(parser, &data, &size);
    if (err == s5_ok) {
        conn_read(incoming);
        return s_handshake_auth;  /* Need more data. */
    }

    if (size != 0) {
        pr_err("junk in request %u", (unsigned) size);
        return do_kill(cx);
    }

    if (err != s5_auth_verify) {
        pr_err("handshake error: %s", s5_strerror(err));
        return do_kill(cx);
    }

    pr_debug("username=%s", parser->username);
    pr_debug("password=%s", parser->password);
    if ( parser->username && 
         parser->password &&
         check_user(pool, cfg->mongo_db, (const char*)parser->username, (const char*)parser->password) == 0)
    {
        conn_write(incoming, "\1\0", 2);  /* auth ok */
        return s_req_start;
    }

    pr_err("auth failed");
    conn_write(incoming, "\1\1", 2);  /* auth failed */
    return do_kill(cx);
}



static int do_req_start(client_ctx *cx) {
    conn *incoming;

    incoming = &cx->incoming;
    ASSERT(incoming->rdstate == c_stop);
    ASSERT(incoming->wrstate == c_done);
    incoming->wrstate = c_stop;

    if (incoming->result < 0) {
        pr_err("write error: %s", uv_strerror(incoming->result));
        return do_kill(cx);
    }

    conn_read(incoming);
    return s_req_parse;
}

static int do_req_parse(client_ctx *cx) {
    conn *incoming;
    conn *outgoing;
    s5_ctx *parser;
    uint8_t *data;
    size_t size;
    int err;

    parser = &cx->parser;
    incoming = &cx->incoming;
    outgoing = &cx->outgoing;
    ASSERT(incoming->rdstate == c_done);
    ASSERT(incoming->wrstate == c_stop);
    ASSERT(outgoing->rdstate == c_stop);
    ASSERT(outgoing->wrstate == c_stop);
    incoming->rdstate = c_stop;

    if (incoming->result < 0) {
        pr_err("read error: %s", uv_strerror(incoming->result));
        return do_kill(cx);
    }

    data = (uint8_t *) incoming->t.buf;
    size = (size_t) incoming->result;
    err = s5_parse(parser, &data, &size);
    if (err == s5_ok) {
        conn_read(incoming);
        return s_req_parse;    /* Need more data. */
    }

    if (size != 0) {
        pr_err("junk in request %u", (unsigned) size);
        return do_kill(cx);
    }

    if (err != s5_exec_cmd) {
        pr_err("request error: %s", s5_strerror(err));
        return do_kill(cx);
    }

    if (parser->cmd == s5_cmd_tcp_bind) {
        /* Not supported but relatively straightforward to implement. */
        pr_warn("BIND requests are not supported.");
        return do_kill(cx);
    }

    if (parser->cmd == s5_cmd_udp_assoc) {
        /* Not supported.    Might be hard to implement because libuv has no
         * functionality for detecting the MTU size which the RFC mandates.
         */
        pr_warn("UDP ASSOC requests are not supported.");
        return do_kill(cx);
    }
    ASSERT(parser->cmd == s5_cmd_tcp_connect);

    if (parser->atyp == s5_atyp_host) {
        conn_getaddrinfo(outgoing, (const char *) parser->daddr);
        return s_req_lookup;
    }

    if (parser->atyp == s5_atyp_ipv6) {
        memset(&outgoing->t.addr6, 0, sizeof(outgoing->t.addr6));
        outgoing->t.addr6.sin6_family = AF_INET6;
        outgoing->t.addr6.sin6_port = htons(parser->dport);
        memcpy(&outgoing->t.addr6.sin6_addr,
                     parser->daddr,
                     sizeof(outgoing->t.addr6.sin6_addr));
    } else {
        return do_kill(cx);
    }

    return do_req_connect_start(cx);
}

static int do_req_lookup(client_ctx *cx) {
    s5_ctx *parser;
    conn *incoming;
    conn *outgoing;

    parser = &cx->parser;
    incoming = &cx->incoming;
    outgoing = &cx->outgoing;
    ASSERT(incoming->rdstate == c_stop);
    ASSERT(incoming->wrstate == c_stop);
    ASSERT(outgoing->rdstate == c_stop);
    ASSERT(outgoing->wrstate == c_stop);

    if (outgoing->result < 0) {
        /* TODO(bnoordhuis) Escape control characters in parser->daddr. */
        pr_err("lookup error for \"%s\": %s",
                     parser->daddr,
                     uv_strerror(outgoing->result));
        /* Send back a 'Host unreachable' reply. */
        conn_write(incoming, "\5\4\0\1\0\0\0\0\0\0", 10);
        return s_kill;
    }

    /* Don't make assumptions about the offset of sin_port/sin6_port. */
    switch (outgoing->t.addr.sa_family) {
        case AF_INET:
            outgoing->t.addr4.sin_port = htons(parser->dport);
            break;
        case AF_INET6:
            outgoing->t.addr6.sin6_port = htons(parser->dport);
            break;
        default:
            UNREACHABLE();
    }

    return do_req_connect_start(cx);
}

/* Assumes that cx->outgoing.t.sa contains a valid AF_INET/AF_INET6 address. */
static int do_req_connect_start(client_ctx *cx) {
    conn *incoming;
    conn *outgoing;
    int err;

    incoming = &cx->incoming;
    outgoing = &cx->outgoing;
    ASSERT(incoming->rdstate == c_stop);
    ASSERT(incoming->wrstate == c_stop);
    ASSERT(outgoing->rdstate == c_stop);
    ASSERT(outgoing->wrstate == c_stop);

    /* TODO: enable it to work somehow
    if (!can_access(cx->sx, cx, &outgoing->t.addr)) {
        pr_warn("connection not allowed by ruleset");
   * Send a 'Connection not allowed by ruleset' reply. *
        conn_write(incoming, "\5\2\0\1\0\0\0\0\0\0", 10);
        return s_kill;
    }
    */
    err = conn_connect(outgoing);
    if (err != 0) {
        pr_err("connect error: %s\n", uv_strerror(err));
        return do_kill(cx);
    }

    return s_req_connect;
}

static int do_req_connect(client_ctx *cx) {
    const struct sockaddr_in6 *in6;
    const struct sockaddr_in *in;
    char addr_storage[sizeof(*in6)];
    conn *incoming;
    conn *outgoing;
    uint8_t *buf;
    int addrlen;

    incoming = &cx->incoming;
    outgoing = &cx->outgoing;
    ASSERT(incoming->rdstate == c_stop);
    ASSERT(incoming->wrstate == c_stop);
    ASSERT(outgoing->rdstate == c_stop);
    ASSERT(outgoing->wrstate == c_stop);

    /* Build and send the reply.    Not very pretty but gets the job done. */
    buf = (uint8_t *) incoming->t.buf;
    if (outgoing->result == 0) {
        /* The RFC mandates that the SOCKS server must include the local port
         * and address in the reply.    So that's what we do.
         */
        addrlen = sizeof(addr_storage);
        CHECK(0 == uv_tcp_getsockname(&outgoing->handle.tcp,
                                                                    (struct sockaddr *) addr_storage,
                                                                    &addrlen));
        buf[0] = 5;    /* Version. */
        buf[1] = 0;    /* Success. */
        buf[2] = 0;    /* Reserved. */
        buf[3] = 1;    /* IPv4. */
        in = (const struct sockaddr_in *) &addr_storage;
        memcpy(buf + 4, &in->sin_addr, 4);
        memcpy(buf + 8, &in->sin_port, 2);
        conn_write(incoming, buf, 10);
        return s_proxy_start;
    } else {
        pr_err("upstream connection error: %s\n", uv_strerror(outgoing->result));
        /* Send a 'Connection refused' reply. */
        conn_write(incoming, "\5\5\0\1\0\0\0\0\0\0", 10);
        return s_kill;
    }

    UNREACHABLE();
    return s_kill;
}

static int do_proxy_start(client_ctx *cx) {
    conn *incoming;
    conn *outgoing;

    incoming = &cx->incoming;
    outgoing = &cx->outgoing;
    ASSERT(incoming->rdstate == c_stop);
    ASSERT(incoming->wrstate == c_done);
    ASSERT(outgoing->rdstate == c_stop);
    ASSERT(outgoing->wrstate == c_stop);
    incoming->wrstate = c_stop;

    if (incoming->result < 0) {
        pr_err("write error: %s", uv_strerror(incoming->result));
        return do_kill(cx);
    }
    /*outgoing->handle.stream.io_watcher.fd = fd;*/
    /*if((bind(fd, (struct sockaddr *) &my_addr6, sizeof(my_addr6))) == -1)
    {
    pr_err("failed to bind: %s", strerror(errno));
    }*/

    conn_read(incoming);
    conn_read(outgoing);
    return s_proxy;
}

/* Proxy incoming data back and forth. */
static int do_proxy(client_ctx *cx) {
    if (conn_cycle("client", &cx->incoming, &cx->outgoing)) {
        return do_kill(cx);
    }

    if (conn_cycle("upstream", &cx->outgoing, &cx->incoming)) {
        return do_kill(cx);
    }

    return s_proxy;
}

static int do_kill(client_ctx *cx) {
    int new_state;

    if (cx->state >= s_almost_dead_0) {
        return cx->state;
    }

    /* Try to cancel the request. The callback still runs but if the
     * cancellation succeeded, it gets called with status=UV_ECANCELED.
     */
    new_state = s_almost_dead_1;
    if (cx->state == s_req_lookup) {
        new_state = s_almost_dead_0;
        pr_debug("%s: trying to cancel request", __func__);
        uv_cancel(&cx->outgoing.t.req);
    }

    conn_close(&cx->incoming);
    conn_close(&cx->outgoing);
    return new_state;
}

static int do_almost_dead(client_ctx *cx) {
    pr_debug("%s state: %d(%s)", __func__, cx->state, strstate(cx->state));
    ASSERT(cx->state >= s_almost_dead_0);
    int ret = cx->state + 1;
    pr_debug("%s returning state: %d", __func__, ret);
    if (!cx->outgoing.addr_is_released)
    {
        free(cx->outgoing.leased_addr);
        cx->outgoing.leased_addr = NULL;
        cx->outgoing.addr_is_released = 1;
    }
    return ret;  /* Another finalizer completed. */
}

static int conn_cycle(const char *who, conn *a, conn *b) {
    if (a->result < 0) {
        if (a->result != UV_EOF) {
          pr_err("%s error: %s", who, uv_strerror(a->result));
        }
        return -1;
    }

    if (b->result < 0) {
        return -1;
    }

    if (a->wrstate == c_done) {
        a->wrstate = c_stop;
    }

    /* The logic is as follows: read when we don't write and write when we don't
     * read.  That gives us back-pressure handling for free because if the peer
     * sends data faster than we consume it, TCP congestion control kicks in.
     */
    if (a->wrstate == c_stop) {
        if (b->rdstate == c_stop) {
            conn_read(b);
        } else if (b->rdstate == c_done) {
            conn_write(a, b->t.buf, b->result);
            b->rdstate = c_stop;  /* Triggers the call to conn_read() above. */
        }
    }

    return 0;
}

static void conn_timer_reset(conn *c) {
    pr_debug("%s: conn: %p state: %d", __func__, (void *)c, c->client->state);
    CHECK(0 == uv_timer_start(&c->timer_handle,
                              conn_timer_expire,
                              c->idle_timeout,
                              0));
}

static void conn_timer_expire(uv_timer_t *handle) {
    conn *c;

    c = CONTAINER_OF(handle, conn, timer_handle);
    pr_debug("Timer expired for conn: %p", (void *)c);
    c->result = UV_ETIMEDOUT;
    c->client->state = s_kill;
    do_next(c->client);
}

static void check_parent_callback(uv_timer_t *handle) {
    int r = uv_kill(parent_pid, 0);
    if (r != 0) {
        uv_stop(loop);
    }
}

static void conn_getaddrinfo(conn *c, const char *hostname) {
    pr_debug("%s: conn: %p state: %d", __func__, (void *)c, c->client->state);
    struct addrinfo hints;

    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_protocol = IPPROTO_TCP;
    CHECK(0 == uv_getaddrinfo(loop,
                              &c->t.addrinfo_req,
                              conn_getaddrinfo_done,
                              hostname,
                              NULL,
                              &hints));
    pr_debug("Resetting getaddrinfo timer for conn: %p", (void *)c);
    conn_timer_reset(c);
}

static void conn_getaddrinfo_done(uv_getaddrinfo_t *req,
                                  int status,
                                  struct addrinfo *ai) {
    conn *c;

    c = CONTAINER_OF(req, conn, t.addrinfo_req);
    pr_debug("%s: conn: %p state: %d", __func__, (void *)c, c->client->state);
    c->result = status;

    if (status == 0) {
        /* FIXME(bnoordhuis) Should try all addresses. */
        if (ai->ai_family == AF_INET) {
            c->t.addr4 = *(const struct sockaddr_in *) ai->ai_addr;
        } else if (ai->ai_family == AF_INET6) {
            c->t.addr6 = *(const struct sockaddr_in6 *) ai->ai_addr;
        } else {
            UNREACHABLE();
        }
    }

    uv_freeaddrinfo(ai);
    do_next(c->client);
}


static int conn_connect(conn *c) {
    char *ip6_addr;
    struct sockaddr_in6 src_addr6;
	int ret;

    ip6_addr = c->leased_addr;
    if (ip6_addr != NULL)
    {
        if ((ret = inet_pton(AF_INET6, ip6_addr, &src_addr6.sin6_addr)) != 1)
        {
            pr_warn("inet_pton: %s", strerror(ret));
        }
    }
    src_addr6.sin6_family = AF_INET6;
    src_addr6.sin6_scope_id = 0;
    size_t i;
    for(i = 0; i < 5; ++i) {
        ret = uv_tcp_bind(&c->handle.tcp, (struct sockaddr *) &src_addr6, 0);
        if (0 != ret)
        {
            pr_warn("bind for custom address '%s' failed: %s (try %ld)",
                    ip6_addr, uv_strerror(ret), i);
            if (UV_EADDRNOTAVAIL == ret) { /* Try to wait for system to apply addr */
                sleep(1);
                continue;
            }
        }
        break;
    }
    if (0 != ret) {
        pr_err("Failed to bind to address '%s' for 5 times", ip6_addr);
        return ret;
    }
    pr_debug("Successfully bind for addresss '%s' after try %ld", ip6_addr, i);

    pr_debug("Resetting connect timer for conn: %p", (void *)c);
    conn_timer_reset(c);
    pr_debug("Timer for conn %p was reset", (void *)c);
    return uv_tcp_connect(&c->connect_req,
                          &c->handle.tcp,
                          &c->t.addr,
                          conn_connect_done);
}

static void conn_connect_done(uv_connect_t *req, int status) {
    conn *c;

    if (status == UV_ECANCELED) {
        return;  /* Handle has been closed. */
    }

    c = CONTAINER_OF(req, conn, connect_req);
    pr_debug("%s: conn: %p state: %d", __func__, (void *)c, c->client->state);
    c->result = status;
    do_next(c->client);
}

static void conn_read(conn *c) {
    pr_debug("%s: conn: %p state: %d rdstate: %d - %s",
             __func__, (void *)c, c->client->state, c->rdstate, strstate(c->rdstate));
    ASSERT(c->rdstate == c_stop);
    CHECK(0 == uv_read_start(&c->handle.stream, conn_alloc, conn_read_done));
    c->rdstate = c_busy;
    pr_debug("Resetting read timer for conn: %p", (void *)c);
    conn_timer_reset(c);
}

static void conn_read_done(uv_stream_t *handle,
                             ssize_t nread,
                             const uv_buf_t *buf) {
    conn *c;

    c = CONTAINER_OF(handle, conn, handle);
    pr_debug("%s: conn: %p state: %d", __func__, (void *)c, c->client->state);
    ASSERT(c->t.buf == buf->base);
    ASSERT(c->rdstate == c_busy);
    c->rdstate = c_done;
    c->result = nread;

    uv_read_stop(&c->handle.stream);
    do_next(c->client);
}

void pipe_alloc(uv_handle_t *handle, size_t suggested_size, uv_buf_t *buf) {
    buf->base = malloc(suggested_size);
    pr_debug("Allocated conn pointer: %p", buf->base);
    buf->len = suggested_size;
}

static void conn_alloc(uv_handle_t *handle, size_t size, uv_buf_t *buf) {
    conn *c;

    c = CONTAINER_OF(handle, conn, handle);
    pr_debug("%s: conn: %p state: %d", __func__, (void *)c, c->client->state);
    ASSERT(c->rdstate == c_busy);
    buf->base = c->t.buf;
    buf->len = sizeof(c->t.buf);
    memset(buf->base, 0, buf->len);
}

static void conn_write(conn *c, const void *data, unsigned int len) {
    pr_debug("%s: conn: %p state: %d", __func__, (void *)c, c->client->state);
    uv_buf_t buf;

    ASSERT(c->wrstate == c_stop || c->wrstate == c_done);
    c->wrstate = c_busy;

    /* It's okay to cast away constness here, uv_write() won't modify the
     * memory.
     */
    buf.base = (char *) data;
    buf.len = len;

    CHECK(0 == uv_write(&c->write_req,
                        &c->handle.stream,
                        &buf,
                        1,
                        conn_write_done));
    pr_debug("Resetting write timer for conn: %p", (void *)c);
    conn_timer_reset(c);
}

static void conn_write_done(uv_write_t *req, int status) {
    conn *c;

    if (status == UV_ECANCELED) {
        pr_debug("%s: Write handle %p has been cancelled", __func__, (void *)req);
        return;  /* Handle has been closed. */
    }

    c = CONTAINER_OF(req, conn, write_req);
    pr_debug("%s: conn: %p state: %d", __func__, (void *)c, c->client->state);
    pr_debug("write_done with status: %d (wrstate: %s)", status, strconnstate(c->wrstate));
    if (c->wrstate == c_dead) {
        return; /* There is nothing we can do */
    }
    ASSERT(c->wrstate == c_busy);
    c->wrstate = c_done;
    c->result = status;
    do_next(c->client);
}

static void conn_close(conn *c) {
    pr_debug("%s: conn: %p state: %d", __func__, (void *)c, c->client->state);
    ASSERT(c->rdstate != c_dead);
    ASSERT(c->wrstate != c_dead);
    c->rdstate = c_dead;
    c->wrstate = c_dead;
    c->timer_handle.data = c;
    c->handle.handle.data = c;
    uv_os_fd_t fd;
    uv_fileno(&c->handle.handle, &fd);
    pr_debug("Closing socket: %d", fd);
    uv_close(&c->handle.handle, conn_close_done);
    uv_close((uv_handle_t *) &c->timer_handle, NULL);
}

static void conn_close_done(uv_handle_t *handle) {
    conn *c;

    c = handle->data;
    pr_debug("%s: conn: %p state: %d", __func__, (void *)c, c->client->state);
    do_next(c->client);
}

static void handle_stop(uv_signal_t *handle, int signum) {
    pr_info("Received signal: %d", signum);
    uv_stop(loop);
}

static void init_signals() {
    static uv_signal_t sigint, sigterm, sigquit;

    uv_signal_init(loop, &sigint);
    uv_signal_start(&sigint, handle_stop, SIGINT);

    uv_signal_init(loop, &sigterm);
    uv_signal_start(&sigterm, handle_stop, SIGTERM);

    uv_signal_init(loop, &sigquit);
    uv_signal_start(&sigquit, handle_stop, SIGQUIT);
}

static void cleanup() {
    pr_debug("Cleaning up");
    mongoc_uri_destroy( uri );
    mongoc_client_pool_destroy( pool );
    mongoc_cleanup();
    free_config();
}

const char *get_prog_name() {
    return progname;
}

