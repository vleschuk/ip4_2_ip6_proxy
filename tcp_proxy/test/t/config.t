use FindBin qw/ $Bin /;
use lib "$Bin/../testlib";
use Test::More qw/ no_plan /;
use Helpers;
use strict;
use warnings;

BEGIN {
    use Proc::Killall;
    killall('TERM', 'tcp_proxy_worker$');
    killall('TERM', 'tcp_proxy$');
    killall('KILL', 'tcp_proxy_worker$');
    killall('KILL', 'tcp_proxy$');
}

END {
    Helpers::stop_proxy('SIGKILL');
}

sub get_num_workers {
    my $t = Proc::ProcessTable->new();
    my @workers = grep { $_->cmndline =~ /tcp_proxy_worker/ } @{$t->table};
    return scalar(@workers);
}

sub get_num_cpus {
    my $cpus = 0;
    open my $fh, "</proc/cpuinfo" or die "Can't open /proc/cpuinfo: $!";
    while(<$fh>) {
        next unless /^processor/;
        ++$cpus;
    }
    close $fh;
    return $cpus;
}

require_ok('Proc::ProcessTable') or BAIL_OUT("Can't load Proc::ProcessTable. Nothing to do");

{
my $n = get_num_workers();

ok(!$n, "no workers before tests");
}

my $cpus =  get_num_cpus();
ok($cpus, "Can get number of cpus");

SKIP: {
    skip "Can't get cpu number", 4 unless $cpus;
    my $err = Helpers::start_proxy({proxy_port => 7000});
    ok(!$err, "Proxy started");
    BAIL_OUT("Can't start proxy") if $err;

    my $n = get_num_workers();
    ok($n == $cpus, "Default number of workers is equal to cpus");

    $err = Helpers::stop_proxy();
    ok(!$err, "Proxy stopped");

    $n = get_num_workers();
    ok(!$n, "No workers after stop");
}

{
    my $err = Helpers::start_proxy({proxy_port => 7000, workers => 1});
    ok(!$err, "Proxy started");
    BAIL_OUT("Can't start proxy") if $err;

    my $n = get_num_workers();
    ok($n == 1, "Number of workers is equal to requested");

    $err = Helpers::stop_proxy();
    ok(!$err, "Proxy stopped");

    $n = get_num_workers();
    ok(!$n, "No workers after stop");
}
